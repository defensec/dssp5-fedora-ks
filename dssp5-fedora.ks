autopart --type=btrfs --noswap --encrypted --passphrase=mypassphrase --luks-version=luks2 --cipher=aes-xts-plain64 --pbkdf-time=5000
bootloader --timeout=1 --append="enforcing=1"
clearpart --all --initlabel --disklabel=msdos
keyboard --vckeymap=us-euro --xlayouts=us
lang en_US.UTF-8
network --bootproto=dhcp --device=link --hostname=myhostname --activate
rootpw myrootpw
shutdown
text
timezone Europe/Amsterdam --utc
url --url=https://ams.edge.kernel.org/fedora-buffet/fedora/linux/development/rawhide/Everything/x86_64/os
zerombr

repo --name=updates
repo --name=dssp5 --baseurl=https://dgrift.home.xs4all.nl/dssp5/

%packages --excludedocs --instLangs=en --ignoremissing --nocore --exclude-weakdeps
argbash
authselect
basesystem
bash
bash-completion
binutils
bluez
brightnessctl
coreutils
cracklib-dicts
createrepo_c
dnf
dnf-plugins-core
dssp5-fedora
dssp5-repos
emacs-nox
fedora-release
filesystem
firefox-wayland
foot
foot-terminfo
fuse-sshfs
fuse3
git
git-email
glibc
grim
install
iproute
kbd
kernel
langpacks-en
less
levien-inconsolata-fonts
libnitrokey
libnotify
make
mako
mesa-dri-drivers
mozilla-fira-sans-fonts
mpc
ncmpc
ncurses
openssh-server
openssl
pam_ssh_agent_auth
pass
passwd
pinentry
pipewire
pipewire-pulseaudio
pulseaudio-utils
pwgen
python3-docutils
python3-libselinux
rootfiles
rpm-plugin-selinux
rpm-sign
rtorrent
secilc
setools-console
setup
shadow-utils
simple-mtpfs
slurp
sudo
sway
sway-systemd
swayidle
swaylock
systemd-container
systemd-networkd
systemd-resolved
systemd-udev
tar
terminus-fonts-console
tig
tmux
util-linux
vim-minimal
waypipe
wget
wireless-regdb
wireplumber
wl-clipboard
wpa_supplicant
zram-generator-defaults
-chrony
%end

%post
systemctl disable dnf-makecache.timer systemd-homed
systemctl enable systemd-resolved systemd-networkd systemd-timesyncd
ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
ln -sf /usr/bin/fusermount3 /usr/bin/fusermount

cat > /etc/selinux/semanage.conf <<EOF
module-store = direct
expand-check = 1
usepasswd = true
ignoredirs=/root
[sefcontext_compile]
path = /usr/sbin/sefcontext_compile
args = -r \$@
[end]
EOF

echo "install_weak_deps=0" >> /etc/dnf/dnf.conf
echo "tsflags=nodocs" >> /etc/dnf/dnf.conf

echo "xwayland disable" > /etc/sway/config.d/xwayland.conf

sed -i 's/#Storage=auto/Storage=volatile/' /etc/systemd/journald.conf
sed -i 's/#ReadKMsg=yes/ReadKMsg=no/' /etc/systemd/journald.conf
sed -i 's/#MaxLevelStore=debug/MaxLevelStore=info/' /etc/systemd/journald.conf
sed -i 's/#MaxLevelSyslog=debug/MaxLevelSyslog=info/' /etc/systemd/journald.conf

sed -i 's/^FONT=.*/FONT="ter-v32n"/' /etc/vconsole.conf

authselect select minimal -f --nobackup

semodule -BNP
restorecon -RF /

mkdir /relabel
mount --bind / /relabel
chcon -u sys.id -r sys.role -t sys.fs -l s0 /relabel/sys
chcon -u sys.id -r sys.role -t run.file -l s0 /relabel/run
chcon -u sys.id -r sys.role -t proc.fs -l s0 /relabel/proc
chcon -u sys.id -r sys.role -t dev.file -l s0 /relabel/dev
chcon -u sys.id -r sys.role -t boot.file -l s0 /relabel/boot
chcon -u sys.id -r sys.role -t tmp.file -l s0 /relabel/tmp
chcon -u sys.id -r sys.role -t home.file -l s0 /relabel/home
rm -f -- /relabel/tmp/ks-script-*
umount /relabel
mount --bind /boot /relabel
chcon -u sys.id -r sys.role -t dos.fs -l s0 /relabel/efi
umount /relabel
mount /dev/mapper/$(dmsetup ls | awk '{ print $1 }') /relabel
chcon -u sys.id -r sys.role -t root.file -l s0 /relabel
umount /relabel
rmdir /relabel

mount -t tmpfs none /tmp
mount -t tmpfs none /root
mount -t tmpfs none /var/log
%end
